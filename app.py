from flask import Flask, jsonify, request
from neo4j import GraphDatabase
from dotenv import load_dotenv
# import os # provides ways to access the Operating System and allows us to read the environment variables

load_dotenv()

app = Flask(__name__)

# uri = os.getenv('URI')
# user = os.getenv("USERNAME")
# password = os.getenv("PASSWORD")
uri = "bolt://localhost:7687"
user = "neo4j"
password = "test1234"
driver = GraphDatabase.driver(uri, auth=(user, password), database="neo4j")


def get_employees(tx, sort_by=None, field=None, value=None):
    query = "MATCH (e:Employee)"

    if field and value:
        query += f" WHERE e.{field} = '{value}'"

    if sort_by:
        query += f" RETURN e ORDER BY e.{sort_by}"
    else:
        query += " RETURN e"

    results = tx.run(query).data()
    employees = [
        {
            'name': result['e']['name'],
            'surname': result['e']['surname'],
            'position': result['e']['position']
        }
        for result in results
    ]
    return employees


@app.route('/employees', methods=['GET'])
def get_employees_route():
    sort_by = request.args.get('sort_by')
    field = request.args.get('field')
    value = request.args.get('value')

    with driver.session() as session:
        employees = session.read_transaction(get_employees, sort_by, field, value)

    response = {'employees': employees}
    return jsonify(response)


def get_employee(tx, id):
    query = "MATCH (e:Employee) WHERE ID(e) = $id RETURN e"
    result = tx.run(query, id=id).data()
    return result[0] if result else None


@app.route('/employees/<int:id>', methods=['GET'])
def get_employee_route(id):
    with driver.session() as session:
        employee = session.read_transaction(get_employee, id)

    if not employee:
        return jsonify({'message': 'Employee not found'}), 404

    return jsonify({'employee': employee['e']}), 200


def add_employee(tx, name, surname, position, department):
    match_query = "MATCH (e:Employee {name: $name, surname: $surname}) RETURN e"
    employee = tx.run(match_query, name=name, surname=surname).data()

    if employee:
        return None
    else:
        create_employee_query = "CREATE (e:Employee {name: $name, surname: $surname, position: $position})"
        tx.run(create_employee_query, name=name, surname=surname, position=position)

        relate_to_department_query = "MATCH (e:Employee {name: $name, surname: $surname}), (d:Department {name: $department}) MERGE (e)-[:WORKS_IN]->(d)"
        tx.run(relate_to_department_query, name=name, surname=surname, department=department)
        return {'name': name, 'surname': surname, 'position': position}


@app.route('/employees', methods=['POST'])
def add_employee_route():
    request_data = request.get_json()

    if 'name' not in request_data or 'surname' not in request_data or 'position' not in request_data or 'department' not in request_data:
        response = {'message': 'Missing parameters. Please provide name, surname, position, and department'}
        return jsonify(response), 400

    name = request_data['name']
    surname = request_data['surname']
    position = request_data['position']
    department = request_data['department']

    with driver.session() as session:
        new_employee = session.write_transaction(add_employee, name, surname, position, department)

    if new_employee:
        response = {'status': 'success'}
        return jsonify(response)
    else:
        response = {'message': 'Employee already exists'}
        return jsonify(response), 404


def update_employee(tx, id, new_name=None, new_surname=None, new_position=None, new_department=None):
    query = "MATCH (e:Employee) WHERE ID(e)=$id RETURN e"
    result = tx.run(query, id=id).data()

    if not result:
        return None
    else:
        set_clauses = []

        if new_name:
            set_clauses.append("e.name = $new_name")

        if new_surname:
            set_clauses.append("e.surname = $new_surname")

        if new_position:
            set_clauses.append("e.position = $new_position")
            if new_position == "Manager":
                change_relationship_query = """
                    MATCH (e:Employee)-[r:WORKS_IN]->(d:Department) WHERE ID(e)=$id DELETE r
                    CREATE (e)-[:MANAGES]->(d)
                """
                tx.run(change_relationship_query, id=id)

        if set_clauses:
            set_query = ", ".join(set_clauses)
            update_query = f"MATCH (e:Employee) WHERE ID(e)=$id SET {set_query}"
            tx.run(update_query, id=id, new_name=new_name, new_surname=new_surname, new_position=new_position)

        if new_department:
            delete_relationship_query = "MATCH (e:Employee)-[r]->() WHERE ID(e)=$id DELETE r"
            tx.run(delete_relationship_query, id=id)

            if new_position == "Manager" or result[0]['e']['position'] == "Manager":
                create_relationship_query = """
                    MATCH (e:Employee), (d:Department {name: $new_department})
                    WHERE ID(e)=$id CREATE (e)-[:MANAGES]->(d)
                """
            else:
                create_relationship_query = """
                    MATCH (e:Employee), (d:Department {name: $new_department})
                    WHERE ID(e)=$id CREATE (e)-[:WORKS_IN]->(d)
                """
            tx.run(create_relationship_query, id=id, new_department=new_department)

        return {'name': new_name, 'surname': new_surname, 'position': new_position, 'department': new_department}


@app.route('/employees/<int:id>', methods=['PUT'])
def update_employee_route(id):
    new_name = request.json.get('name')
    new_surname = request.json.get('surname')
    new_position = request.json.get('position')
    new_department = request.json.get('department')

    with driver.session() as session:
        employee = session.write_transaction(update_employee, id, new_name, new_surname, new_position, new_department)

    if not employee:
        response = {'message': 'Employee not found'}
        return jsonify(response), 404
    else:
        response = {'status': 'success'}
        return jsonify(response)


def delete_employee(tx, id):
    query = "MATCH (e:Employee) WHERE ID(e)=$id RETURN e"
    result = tx.run(query, id=id).data()

    if not result:
        return None
    else:
        employee = result[0]['e']
        employee_name = f"{employee['name']} {employee['surname']}"

        if employee['position'] == 'Manager':
            print(f"{employee_name} is a manager. Looking for a new manager...")

            query_subordinates = """
            MATCH (e:Employee)-[:MANAGES]->(d:Department)<-[:WORKS_IN]-(s:Employee)
            WHERE ID(e)=$id RETURN ID(s) AS new_manager_id, s, d
            """
            result_subordinates = tx.run(query_subordinates, id=id).data()

            query_delete_employee = "MATCH (e:Employee) WHERE ID(e)=$id DETACH DELETE e"
            tx.run(query_delete_employee, id=id)
            department = result_subordinates[0]['d']

            if result_subordinates:
                new_manager = result_subordinates[0]['s']
                new_manager_id = result_subordinates[0]['new_manager_id']

                query_set_new_manager = """
                MATCH (s:Employee)-[r:WORKS_IN]->(d:Department) WHERE ID(s)=$new_manager_id
                DELETE r
                SET s.position = 'Manager'
                MERGE (s)-[:MANAGES]->(d)
                """
                tx.run(query_set_new_manager, new_manager_id=new_manager_id)
                new_manager_name = f"{new_manager['name']} {new_manager['surname']}"
                print(f"{new_manager_name} is now the manager of {department['name']} department.")
                return {'id': id}
            else:
                query_delete_department = "MATCH (d:Department) WHERE ID(d)=ID($department) DETACH DELETE d"
                tx.run(query_delete_department, id=id)
        else:
            query_delete_employee = "MATCH (e:Employee) WHERE ID(e)=$id DETACH DELETE e"
            tx.run(query_delete_employee, id=id)
            return {'id': id}


@app.route('/employees/<int:id>', methods=['DELETE'])
def delete_employee_route(id):
    with driver.session() as session:
        employee = session.write_transaction(delete_employee, id)

    if not employee:
        response = {'message': 'Employee not found'}
        return jsonify(response), 404
    else:
        response = {'status': 'success'}
        return jsonify(response)


def get_subordinates(tx, id):
    query = "MATCH (s:Employee)-[:WORKS_IN]->(d:Department)<-[:MANAGES]-(m:Employee) WHERE ID(m)=$id RETURN s"
    results = tx.run(query, id=id).data()
    subordinates = [
        {
            'name': result['s']['name'],
            'surname': result['s']['surname'],
            'position': result['s']['position']
        }
        for result in results
    ]
    return subordinates


@app.route('/employees/<int:id>/subordinates', methods=['GET'])
def get_subordinates_route(id):
    with driver.session() as session:
        subordinates = session.read_transaction(get_subordinates, id)

    response = {'subordinates': subordinates}
    return jsonify(response)


def get_departments(tx, field=None, value=None):
    query = "MATCH (m:Employee)-[:MANAGES]->(d:Department)<-[:WORKS_IN]-(e:Employee) WITH d, m, count(e) AS num"

    if field and value:
        if field == "num":
            query += f" WHERE num {value}"
            # if you want to filter by the number of employees, pass =, >, < and a number as the "value"
        else:
            query += f" WHERE d.{field} = '{value}'"

    query += " RETURN d, m, num"
    results = tx.run(query).data()
    departments = [{'name': result['d']['name'], 'manager': result['m']['name']+" "+result['m']['surname'],  'number_of_employees': result['num']} for result in results]
    return departments


@app.route('/departments', methods=['GET'])
def get_departments_route():
    field = request.args.get('field')
    value = request.args.get('value')

    with driver.session() as session:
        departments = session.read_transaction(get_departments, field, value)

    response = {'departments': departments}
    return jsonify(response)


def get_departments_employees(tx, id):
    query = "MATCH (e:Employee)-[:WORKS_IN]->(d:Department)<-[:MANAGES]-(m:Employee) WHERE ID(d)=$id RETURN e, m"
    results = tx.run(query, id=id).data()
    departments_employees = [
        {
            'name': result['e']['name'],
            'surname': result['e']['surname'],
            'position': result['e']['position']
        }
        for result in results
    ]
    departments_manager = {
            'name': results[0]['m']['name'],
            'surname': results[0]['m']['surname'],
            'position': results[0]['m']['position']
        }
    departments_employees.append(departments_manager)
    return departments_employees


@app.route('/departments/<int:id>/employees', methods=['GET'])
def get_departments_employees_route(id):
    with driver.session() as session:
        departments_employees = session.read_transaction(get_departments_employees, id)

    response = {'departments_employees': departments_employees}
    return jsonify(response)


def get_employees_department(tx, id):
    query = """
         MATCH (e:Employee)-[:WORKS_IN]->(d:Department)
         WHERE ID(e)=$id
         MATCH (n:Employee)-[:WORKS_IN]->(d)<-[:MANAGES]-(m:Employee) RETURN count(n) AS number, d, m
    """
    results = tx.run(query, id=id).data()
    if not results:
        return None
    department_info = {
        "department": results[0]['d']['name'],
        "manager": results[0]['m']['name']+" "+results[0]['m']['surname'],
        "number_of_employees": results[0]['number']
    }
    return department_info


@app.route('/employees/<int:id>/department', methods=['GET'])
def get_employees_department_route(id):
    with driver.session() as session:
        department_info = session.read_transaction(get_employees_department, id)
    if not department_info:
        response = {'message': 'Department info not found'}
        return jsonify(response), 404
    else:
        response = {'department_info': department_info}
        return jsonify(response)


if __name__ == '__main__':
    app.run()

